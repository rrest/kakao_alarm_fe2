import React, { Component } from 'react';

import Alarm from './alarm';

class Body extends Component {
  render() {
    return (
      <div style={{ marginTop: 80 }}>
        <Alarm />
      </div>
    );
  }
}

export default Body;