import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';

import { AlarmActions } from 'store/actionCreators';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  }
});

class Add extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  handleAddPopup = () => {
    AlarmActions.setUpdateData();
    AlarmActions.setAlarmMode(1);
  }

  render() {
    const { classes } = this.props;

    return (
      <Button 
        variant="contained" 
        color="primary" 
        className={classes.button}
        onClick={this.handleAddPopup}
      >
        <b>작업 추가</b>
        <AddIcon />
      </Button>
    );
  }
}

export default connect(state => ({
  mode: state.alarm.mode
}))(withStyles(styles)(Add));