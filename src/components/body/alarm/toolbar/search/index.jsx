import React, { Component, Fragment } from 'react';

import Category from './category';
import Keyword from './keyword';

class Search extends Component {
  render() {
    return (
      <Fragment>
        <Category />
        <Keyword />
      </Fragment>
    );
  }
}

export default Search;