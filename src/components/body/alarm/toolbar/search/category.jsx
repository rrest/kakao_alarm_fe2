import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { AlarmActions } from 'store/actionCreators';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    height: 64
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2
  }
});

class Category extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  handleChange = ({target}) => {
    AlarmActions.setSearchCategory(target.value);
  };

  render() {
    const { classes, category } = this.props;
    const menuList = [{
      value: 'username', 
      title: '사용자명'
    }, {
      value: 'subject', 
      title: '작업명'
    }, {
      value: 'message', 
      title: '메세지 내용'
    }];

    return (
      <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="search-category">Search Field</InputLabel>
          <Select
            value={ category || menuList[0].value }
            onChange={ this.handleChange }
            inputProps={{ name: 'category', id: 'search-category' }}
          >
            { menuList.map((obj, index) => <MenuItem key={index} value={obj.value}>{obj.title}</MenuItem>) }
          </Select>
        </FormControl>
      </form>
    );
  }
}

export default connect(
  state => ({
    category: state.alarm.searchCategory
  })
)(withStyles(styles)(Category));