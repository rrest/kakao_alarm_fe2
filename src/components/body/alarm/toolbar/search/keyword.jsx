import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

import { AlarmActions } from 'store/actionCreators';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    height: 64
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200
  }
});

class Keyword extends React.Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  handleChange = ({ target }) => {
    AlarmActions.setSearchKeyword(target.value);
  };

  render() {
    const { classes, keyword } = this.props;

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          label="Search Keyword"
          placeholder="검색 키워드"
          className={ classes.textField }
          value={ keyword }
          onChange={ this.handleChange }
          margin="normal"
          InputLabelProps={{
            shrink: true
          }}
        />
      </form>
    );
  }
}

export default connect(
  state => ({
    keyword: state.alarm.searchKeyword
  })
)(withStyles(styles)(Keyword));