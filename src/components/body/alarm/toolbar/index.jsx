import React, { Component } from 'react';

import AppBar from '@material-ui/core/AppBar';
import ToolbarArea from '@material-ui/core/Toolbar';

import Search from './search';
import Add from './add';

class Toolbar extends Component {
  content = () => {
    return <Search />;
  }

  render() {
    return (
      <AppBar position="static" color="default" >
        <ToolbarArea>
          <Search />
          <Add />
        </ToolbarArea>
      </AppBar>
    );
  }
}

export default Toolbar;