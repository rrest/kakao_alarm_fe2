import React, { Component, Fragment } from 'react';

import Toolbar from './toolbar';
import List from './list';
import Popup from './popup';

class Alarm extends Component {
  render() {
    return (
      <Fragment>
        <Toolbar />
        <List />
        <Popup />
      </Fragment>
    );
  }
}

export default Alarm;