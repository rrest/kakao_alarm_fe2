import React, { Component } from 'react';
import { connect } from 'react-redux';

import Table from 'components/template/table';
import { getAlarmList, updateAlarm } from 'common/feature';
import { AlarmActions } from 'store/actionCreators';

class List extends Component {

  handleStatus = data => {
    /* eslint-disable */
    if (!confirm('현재 알람을 동작을 중지 합니다.\n\n계속 하시겠습니까?')) {
      return false;
    }
    /* eslint-enable */

    updateAlarm({ ...data, status: (data.status ? 0 : 1) }, () => {
      getAlarmList(data => {
        AlarmActions.setAlarmList(data.content);
      });
    });
  }

  handleUpdate = data => {
    AlarmActions.setUpdateData(data);
    AlarmActions.setAlarmMode(2);
  }

  render() {
    const { headerList, alarmList, searchCategory, searchKeyword, userId } = this.props;

    return (
      <Table 
        header={ headerList.filter(obj => obj.active) } 
        alarmList={ (searchKeyword !== '' ? alarmList.filter(obj => ~obj[searchCategory].indexOf(searchKeyword)) : alarmList) }
        handleUpdate={this.handleUpdate}
        handleStatus={this.handleStatus}
        userId={userId}
      />
    );
  }
}

export default connect( state => ({
  headerList: state.alarm.headerList,
  alarmList: state.alarm.alarmList,
  searchCategory: state.alarm.searchCategory,
  searchKeyword: state.alarm.searchKeyword,
  userId: state.user.userId
}))(List);