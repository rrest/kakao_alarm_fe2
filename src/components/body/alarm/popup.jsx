import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import Dialogs from '../../template/dialogs';
import Alarm from 'common/alarm';
import { AlarmActions } from 'store/actionCreators';
import { getAlarmList, insertAlarm, updateAlarm, deleteAlarm } from 'common/feature';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class Popup extends Component {
  state = {
    ...(new Alarm()), username: this.props.username, userId: this.props.userId
  }

  componentWillReceiveProps(nextProps) {
    const { updateData } = nextProps;

    if (this.props.mode !== nextProps.mode) {
      this.setState({
        ...(new Alarm()), 
        ...updateData, 
        userId: nextProps.userId, 
        username: nextProps.username
      });

      return true;
    }

    return false;
  }

  isValid = () => {
    if ( typeof this.state.subject !== 'string' || this.state.subject.trim().length === 0 ) {
      alert('알람 제목을 입력해주세요');
      return false;
    }

    if ( typeof this.state.message !== 'string' || this.state.message.trim().length === 0 ) {
      alert('알람 내용을 입력해주세요');
      return false;
    }
    
    return true;
  }

  handleSave = () => {
    if (this.isValid()) {
      insertAlarm({
        ...this.state
      }, data => {
        AlarmActions.setAddAlarm(data);
  
        AlarmActions.setAlarmMode(0);
      });
    }
  }

  handleUpdate = () => {
    /* eslint-disable */
    if (!confirm('현재 알람을 입력된 정보로 수정합니다.\n\n이 행동은 취소할 수 없습니다. 계속 하시겠습니까?')) {
      return false;
    }
    /* eslint-enable */

    updateAlarm(this.state, () => {
      getAlarmList(data => {
        AlarmActions.setAlarmList(data.content);

        AlarmActions.setAlarmMode(0);
      });
    });
  }

  handleDelete = () => {
    /* eslint-disable */
    if (!confirm('현재 알람을 삭제 합니다\n\n이 행동은 취소할 수 없습니다. 계속 하시겠습니까?')) {
      return false;
    }
    /* eslint-enable */

    deleteAlarm({ids: [ this.state.id ]}, resObj => {
      if (resObj) {
        getAlarmList(data => {
          AlarmActions.setAlarmList(data.content);
  
          AlarmActions.setAlarmMode(0);
        });
      } else {
        alert('삭제 요청이 정상적으로 처리되지 않았습니다.\n\n관리자에게 문의 바랍니다.');
      }
    });
  }

  handleClose = () => {
    AlarmActions.setAlarmMode(0);
  }

  handleUpdateState = e => {
    const id = e.target.id;
    const value = e.target.value;

    if (this.state.hasOwnProperty(id)) {
      this.setState({ ...this.state, [id]: value});
    } else if (this.state.unitType.hasOwnProperty(id)) {
      this.setState({ 
        ...this.state, 
        unitType: {
          ...this.state.unitType,
          [id]: value
        }
      });
    }
  }

  dialogContent = () => {
    const { mode } = this.props;
    const textField = {
      marginRight: 10
    };

    switch (mode) {
      case 1: break;
      case 2: break;
      default: break;
    }

    return (
      <Fragment>
        <TextField margin="dense" id="subject" ref="subject" label="제목" value={this.state.subject} onChange={this.handleUpdateState} fullWidth autoFocus />
        <TextField margin="dense" id="message" label="메세지" value={this.state.message} onChange={this.handleUpdateState} fullWidth />
        <TextField margin="dense" id="year" label="년" value={this.state.unitType.year} style={textField} onChange={this.handleUpdateState} />
        <TextField margin="dense" id="month" label="월" value={this.state.unitType.month} style={textField} onChange={this.handleUpdateState} />
        <TextField margin="dense" id="dayOfWeek" label="요일" value={this.state.unitType.dayOfWeek} style={textField} onChange={this.handleUpdateState} />
        <TextField margin="dense" id="dayOfMonth" label="일" value={this.state.unitType.dayOfMonth} style={textField} onChange={this.handleUpdateState} />
        <TextField margin="dense" id="hours" label="시" value={this.state.unitType.hours} style={textField} onChange={this.handleUpdateState} />
        <TextField margin="dense" id="minutes" label="분" value={this.state.unitType.minutes} style={textField} onChange={this.handleUpdateState} />
      </Fragment>
    );
  }

  dialogAction = () => {
    return (
      <Fragment>
        <Button onClick={this.handleClose} color="primary">취소</Button>
        { this.props.mode === 1 && <Button onClick={this.handleSave} color="primary">저장하기</Button> }
        { this.props.mode === 2 && <Button onClick={this.handleUpdate} color="primary">수정하기</Button> }
        { this.props.mode === 2 && <Button onClick={this.handleDelete} color="secondary">삭제하기</Button> }
      </Fragment>
    );
  };

  render() {
    return (
      <Dialogs onClose={this.handleClose}
        open={this.props.mode > 0}
        title={this.props.mode % 2 ? '새 알람 추가하기' : '알람 수정'}
        content={this.dialogContent()}
        action={this.dialogAction()}
      />
    );
  }
}

export default connect(state => ({
  mode: state.alarm.mode, 
  updateData: state.alarm.updateData,
  userId: state.user.userId,
  username: state.user.username
}))(Popup);