import React, { Component, Fragment } from 'react';

import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';

import { kakaoLogin, getAlarmList } from '../common/feature';
import { UserActions, AlarmActions } from 'store/actionCreators';
import Dialogs from './template/dialogs';


function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Login extends Component {
  state = {
    isLogin: false
  }

  componentWillMount() {
    try {
      const userInfo = JSON.parse(sessionStorage.getItem('userInfo'));

      if (userInfo) {
        UserActions.setUserInfo(userInfo);

        this.setState({...this.state, isLogin: true});

        getAlarmList(({ content }) => AlarmActions.setAlarmList(content));
      }
    } catch (e) { 
      throw Error(e); 
    };
  }

  login = () => {
    kakaoLogin(data => {
      const userInfo = {
        userId: data.userId, 
        userImage: data.userImage,
        username: data.username, 
        userToken: data.tokenType + ' ' + data.accessToken 
      };
      sessionStorage.setItem('userInfo', JSON.stringify(userInfo));

      UserActions.setUserInfo(userInfo);

      this.setState({...this.state, isLogin: true});
      
      getAlarmList(data => AlarmActions.setAlarmList(data.content));
    });
  };

  dialogContent = () => {
    return (
      <DialogContentText id="alert-dialog-slide-description">
        카카오 로그인을 통하여 개인정보 제공 가능 합니다.
      </DialogContentText>
    );
  };

  dialogAction = () => {
    return (
      <Fragment>
        <Button onClick={this.login} color="primary">확인</Button>
        <Button onClick={this.login} color="primary">승인</Button>
      </Fragment>
    );
  };

  render() {
    const { isLogin } = this.state;

    return <Dialogs 
      open={!isLogin} 
      title='카카오 로그인'
      content={this.dialogContent()}
      action={this.dialogAction()}
      transition={Transition}
    />;
  }
}

export default Login;