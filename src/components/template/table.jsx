import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Edit from '@material-ui/icons/Edit';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  update: {
    margin: 0,
    padding: theme.spacing.unit
  },
  table: {
    minWidth: 700
  }, 
  tdh: {
    cursor: 'default'
  },
  progress: {
    margin: theme.spacing.unit * 2
  }
});

class TableTemplate extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired
  }

  render() {
    const { classes, header, alarmList, handleUpdate, handleStatus, userId } = this.props;

    return (
      <Paper className={classes.root}>
        <Table className={classes.table} padding="dense" >
          <TableHead>
            <TableRow>
              <TableCell align="center" style={{fontWeight: 'bold', fontSize: 14}}>상태</TableCell>
              { 
                header.map((obj, index) => <TableCell key={index} align="center" style={{fontWeight: 'bold', fontSize: 14}}>{obj.title}</TableCell>) 
              }
              <TableCell align="center" style={{fontWeight: 'bold', fontSize: 14}}>수정</TableCell>
            </TableRow>
          </TableHead>
          <TableBody> 
            { 
              alarmList.map(row => {
                return (
                  <TableRow key={row.id} hover >
                    <TableCell className={classes.tdh} align="center">
                      <IconButton className={classes.update} size="small" disabled={userId !== row.userId && true} onClick={handleStatus.bind(null, row)}>
                        <img alt={row.status ? '진행' : '멈춤'} src={row.status ? '/favicon_fast.ico' : '/favicon.ico'}/>
                      </IconButton>
                    </TableCell>
                    {
                      header.map((obj, index) => (
                        row.hasOwnProperty(obj.value) ? 
                          <TableCell key={index} className={classes.tdh} component="th" scope="row" align="center">{row[obj.value]}</TableCell> : 
                          <TableCell key={index} className={classes.tdh} align="center">{row.unitType[obj.value]}</TableCell>
                      ))
                    }
                    <TableCell className={classes.tdh} align="center">
                      <IconButton className={classes.update} size="small" disabled={userId !== row.userId && true} onClick={handleUpdate.bind(null, row)}>
                        <Edit />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                );
              })
            } 
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default withStyles(styles)(TableTemplate);