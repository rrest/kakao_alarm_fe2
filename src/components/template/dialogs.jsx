import React from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class AlertDialogSlide extends React.Component {
  static defaultProps = {
    open: false
  }

  static propTypes = {
    title: PropTypes.string,
    open: PropTypes.bool
  }

  render() {
    return (
      <div>
        <Dialog
          open={this.props.open}
          TransitionComponent={this.props.transition}
          keepMounted
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">
            {this.props.title}
          </DialogTitle>
          <DialogContent>
            {this.props.content}
          </DialogContent>
          <DialogActions>
            {this.props.action}
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default AlertDialogSlide;