import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1,
    cursor: 'default'
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  chip: {
    margin: theme.spacing.unit
  }
});

class Header extends Component {
  static propTypes = {
    classes: PropTypes.object,
    username: PropTypes.string, 
    userImage: PropTypes.string
  }

  render() {
    const { classes, username, userImage } = this.props;

    const getAvatar = () => {
      if (userImage) {
        return <Avatar alt={username} src={userImage} />;
      } else {
        return <Avatar alt={username}>{username.slice(0, 1)}</Avatar>;
      }
    };

    return (
      <div className={classes.root}>
        <AppBar position="fixed" color="primary">
          <Toolbar>
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Genius for KAKAO
            </Typography>
            <Chip
              avatar={ getAvatar() }
              label={username}
              className={classes.chip}
              color="default"
            />
          </Toolbar>
        </AppBar>
      </div>
    );
  };
}

export default connect(
  state => ({
    username: state.user.username, 
    userImage: state.user.userImage
  })
)(withStyles(styles)(Header));