import React, { Component, Fragment } from 'react';
import { Provider } from 'react-redux';

import store from '../store';

import Login from './login';
import Header from './header';
import Body from './body';

class Components extends Component {
  render() {
    return (
      <Provider store={store}>
        <Fragment>
          <Header />
          <Body />
          <Login />
        </Fragment>
      </Provider>
    );
  }
};

export default Components;