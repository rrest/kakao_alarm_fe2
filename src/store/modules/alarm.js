import { createAction, handleActions } from 'redux-actions';
const SET_PROPERTY = 'SET_PROPERTY';
const SET_ALARM_MODE = 'SET_ALARM_MODE';
const SET_ALARM_LIST = 'SET_ALARM_LIST';
const SET_ADD_ALARM = 'SET_ADD_ALARM';
const SET_SEARCH_CATEGORY = 'SET_SEARCH_CATEGORY';
const SET_SEARCH_KEYWORD = 'SET_SEARCH_KEYWORD';
const SET_UPDATE_DATA = 'SET_UPDATE_DATA';

export const setProperty = createAction(SET_PROPERTY);
export const setAlarmMode = createAction(SET_ALARM_MODE);
export const setAlarmList = createAction(SET_ALARM_LIST);
export const setAddAlarm = createAction(SET_ADD_ALARM);
export const setSearchCategory = createAction(SET_SEARCH_CATEGORY);
export const setSearchKeyword = createAction(SET_SEARCH_KEYWORD);
export const setUpdateData = createAction(SET_UPDATE_DATA); 

export default handleActions({
  [SET_PROPERTY]: (state, { payload }) => {
    return { ...state, ...payload };
  },
  [SET_ALARM_MODE]: (state, { payload }) => {
    return { ...state, mode: payload };
  },
  [SET_ALARM_LIST]: (state, { payload }) => {
    return { ...state, alarmList: payload };
  },
  [SET_ADD_ALARM]: (state, { payload }) => {
    return { ...state, alarmList: [ ...state.alarmList, payload ]};
  },
  [SET_SEARCH_CATEGORY]: (state, { payload }) => {
    return { ...state, searchCategory: payload };
  }, 
  [SET_SEARCH_KEYWORD]: (state, { payload }) => {
    return { ...state, searchKeyword: payload };
  }, 
  [SET_UPDATE_DATA]: (state, { payload }) => {
    return { ...state, updateData: (payload || {}) };
  }
}, {
  /**
   * mode 
   * 0 : normal
   * 1 : insert
   * 2 : update
   */
  mode: 0,
  searchCategory: 'subject',
  searchKeyword: '',
  headerList: [{
    title: '상태', value: 'status', active: false
  }, {
    title: '사용자명', value: 'username', active: true
  }, {
    title: '알람제목', value: 'subject', active: true
  }, {
    title: '년도', value: 'year', active: true
  }, {
    title: '월', value: 'month', active: true
  }, {
    title: '요일', value: 'dayOfWeek', active: true
  }, {
    title: '일', value: 'dayOfMonth', active: true
  }, {
    title: '시', value: 'hours', active: true
  }, {
    title: '분', value: 'minutes', active: true
  }], 
  alarmList: [], 
  updateData: {}
});