import { createAction, handleActions } from 'redux-actions';

// 액션 타입
const SET_USER_INFO = 'SET_USER_INFO';

// 액션 함수
export const setUserInfo = createAction(SET_USER_INFO);

export default handleActions({
  [SET_USER_INFO]: (state, { payload }) => {
    return { ...state, ...payload };
  }
}, {
  userId: -1,
  userImage: '',
  username: '',
  userToken: null
});