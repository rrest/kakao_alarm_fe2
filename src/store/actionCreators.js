import { bindActionCreators } from 'redux';

import * as userAction from './modules/user';
import * as alarmActions from './modules/alarm';

import store from './index';

const { dispatch } = store;

export const UserActions = bindActionCreators(userAction, dispatch);
export const AlarmActions = bindActionCreators(alarmActions, dispatch);