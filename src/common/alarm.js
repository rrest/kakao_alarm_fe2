export default class Alarm {
  constructor() {
    /** 
     * @name subject 알람 타이틀
     * @default ''
     * @type {string} 
     **/
    this.subject = '';
    
    /** 
     * @name message 알람 메세지
     * @default ''
     * @type {string} 
     **/
    this.message = '';

    /** 
     * @name status 상태여부
     * @default 1
     * @type {number} 
     **/
    this.status = 1;
    
    /** 
     * @name id 알람 프로세스 아이디
     * @default {hash}
     * @type {string} 
     **/
    this.id = '';

    /** 
     * @name userId 사용자 (아이디)
     * @default ''
     * @type {string} 
     **/
    this.userId = '';

    /** 
     * @name username 사용자 (명)
     * @default ''
     * @type {string} 
     **/
    this.username = '';

    this.unitType = {
      /** 
       * @name year 연도
       * @default '*'
       * @type {string} 
       **/
      year: '*',
      /** 
       * @name month 월
       * @default '*'
       * @type {string} 
       **/
      month: '*',
      /** 
       * @name dayOfMonth 일
       * @default '?'
       * @type {string} 
       **/
      dayOfMonth: '?',
      /** 
       * @name dayOfWeek 요일
       * @default '*'
       * @type {array} 
       **/
      dayOfWeek: '*',
      /** 
       * @name hours 시
       * @default '*'
       * @type {string} 
       **/
      hours: '*',
      /** 
       * @name minutes 분
       * @default '*'
       * @type {string} 
       **/
      minutes: '*'
    };
  }
}