import axios from 'axios';
import { server, user, alarm } from './config';

const getUrl = type => {
  let url = server.host + ':' + server.port;

  switch (type) {
    case 'user':
      return url += user.path;
    default: 
      if (alarm[type]) {
        return url += alarm[type].path + (alarm[type].args ? '?size=30&page=1&sort=id,asc' : '');
      } else {
        throw Error('undefined URL Type');
      }
  }
};

const procAjaxCall = (url, method = 'get', data = {}, callback) => {
  axios({
    method, 
    url, 
    data, 
    headers: (jsonStr => {
      if (jsonStr) {
        const userInfo = JSON.parse(jsonStr);
        return { 'Authorization': userInfo.userToken };
      } else {
        return { 'Authorization': '' };
      }
    })(sessionStorage.getItem('userInfo'))
  })
    .then(response => {
      if (+response.status === 200 && response.data) {
        callback(response.data);
      }
    })
    .catch(response => {
      /* eslint-disable no-console */
      console.error(Error(response));
      /* eslint-enable no-console */
    });
};

export const getUserAuth = (data, callback) => {
  procAjaxCall( getUrl('user'), 'post', data, callback );
};

export const kakaoLogin = (callback) => {
  const kakaoJSKey = '79f80a6448bd575ba743ed1534426c0c';
  /* eslint-disable */
  
  Kakao.init(kakaoJSKey);
  Kakao.Auth.login({
    success: authObj => {
      getUserAuth(authObj, callback);
    },
    fail: error => {
      alert(JSON.stringify(error));
    }, 
    always: () => {},
    persistAccessToken: true,
    persistRefreshToken: false,
    throughTalk: true
  });
  /* eslint-enable */
};

export const getAlarmList = callback => {
  procAjaxCall(getUrl('list'), 'get', null, callback);
};

export const insertAlarm = (data, callback) => {
  procAjaxCall(getUrl('save'), 'post', data, callback);
};

export const updateAlarm = (data, callback) => {
  procAjaxCall(getUrl('update'), 'post', data, callback);
};

export const deleteAlarm = (data, callback) => {
  procAjaxCall(getUrl('delete'), 'post', data, callback);
};