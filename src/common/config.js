export const server = ((env) => {
  switch (env && process.env.REACT_APP_MODE) {
    case 'production': 
      return {
        host: 'http://alimy.choibom.com',
        port: '80'
      };
    case 'development': 
    default:
      return {
        host: 'http://localhost',
        port: '9030'
      };
  }
})(process.env.NODE_ENV);

export const user = {
  path: '/auth/'
};

export const alarm = {
  list: {
    path: '/alimy',
    args: {
      size: 30,
      page: 1, 
      sort: 'id,asc'
    }
  },
  save: {
    path: '/alimy/save'
  },
  update: {
    path: '/alimy/update'
  },
  delete: {
    path: '/alimy/delete'
  }
};